# <u>An agent-based model to analyze the dynamic and the computational properties of the Slime Mould</u> 

Undergraduate thesis project in Computer Science at UNAM.

### Collaborators
- Alejandro  T. Valderrama Silva
- *Advisor:* Gustavo Carreón Vázquez

## About 

The "Physarum Polycephalum", better known as Slime Mould, is a unicellular organism belonging to the Mycetozoa group ("animal-fungoid" organisms). In this thesis project, the dynamics of P. Polycephalum, in its plasmodium facet, will be analyzed so that a computational model is defined and developed inspired by its behavior. In recent years it has turned out to be of interest in Computer Science's areas, due to its ability to solve different optimization problems such as: the shortest path problem in configurations within countries [1,2,3]; solving mazes of different difficulties [4]; problems in the area of computational geometry, just as the concave and convex hull [5]; or even a stochastic optimization method [6] that has been used for different engineering solutions.

The use of computational models and simulations from a Computer Science perspective, along with the Agent-Based Modeling (ABM) methodology [7],[8], is proposed to perform the analysis of the dynamics of the Slime Mould. The purpose of this analysis is to extract a set of decentralized rules of evolution with a bottom-up approach, since this organism shows two scales of organization: a local one, where the development of the organism is observed with respect to its immediate environment like food, obstacles and environment variables; and a global one, where is visible the construction of complex patterns that are useful for solving problems. In addition, a biological and computational [9] approach will be established to understand the process of growth and exploration in the organism.

## Approach 

In this thesis work, the behavior and properties that the Slime Mold shows when facing its environment are analyzed. Although the composition of a system of this type is important, what is really relevant is the dynamics and patterns that are generated, since they can be used to solve a great variety of problems. This is supported in several experiments where it is observed that the Slime presents a dynamic capable of solving certain computational problems (such as those mentioned above). In other words, the interest in this organism arises due to his ability to optimize non-trivial problems in a decentralized way, such as finding the shortest path in a graph.

It is possible to identify several important characteristics in the behavior of the Slime Mould for the construction of the model, for instance: *emergence*, the slime changes its stage, within its life cycle, based on the interaction of the cells and the environment conditions; *adaptation*, modifies its behavior due to the conditions of their environment; *collective behavior*, the elements that make it up work towards the same goal; *decentralization*, the body is made up of many elements that act autonomously.
These characteristics allow us to conclude that it is possible to use a bottom-up approach for the construction of the model. By using this approach, special attention is paid to the individual components of the system and their interactions; the purpose is to specify these components as they are linked to form larger structures on a higher organizational scale.

From the analysis and observation of this organism, its dynamic properties will be determined in order to carry out an abstraction process to define a computational model. Subsequently, the ABM methodology will be used to define the characteristics of the agents and the local rules of interaction, this will define the computational model to, roughly, replicate the dynamics of the Physarum Polycephalum, preserving the properties of growth and exploration in the environment. Through the simulation of the model, it will be possible to measure its performance and test it in different scenarios. The model will be implemented in the **NetLogo** [10] development environment.

## Objectives

Develop an agent-based model inspired by the behavior of the Physarum Polycephalum in its plasmodium phase to replicate its dynamics.
Specifically:

- Analyze the dynamics of the Slime Mould to propose a set of local rules that guide the behavior of the agents.
- Obtain a computational model that rescues the dynamic properties of the organism.
- Compare the results, through the simulation of the model, against the performance shown in experiments already developed with the biological organism, such as the search for the shortest path in graphs and solution of mazes.
- Analyze the model under simulated scenarios with the following elements: 1) a predefined distribution of the food in the environment; 2) obstacles that represent regions in the environment that the Slime cannot pass through and 3) repellent regions due to environmental conditions.

## Main structure of the thesis

1. Introduction
2. Foundation 
   1. The biology of the *P. Polycephalum*
   2. Complex Systems
   3. ABM as applied methodology
   4. Models inspired in biological systems
3. Computational Model of the *P. Polycephalum*
   1. Introduction to the *P. Polycephalum* experiments
   2. Abstraction and development of the model
   3. Model's dynamics.
4. Results of model's dynamic
   1. Dynamics with pre configuration of food
   2. Dynamics with introduction of repellent agents 
   3. Dynamics with introduction of obstacles
5. Conclutions

## References

1. Atsushi Tero, Seiji Takagi, et al (2010). ''Rules for Biologically Inspired Adaptive Network Design''. Science, No. 327, Vol. 439 : 439 - 442.
2. Andrew Adamatzky, Martínez G.J., Chapa-Vergara S.V., et al (2011). Approximating Mexican highways with slime mould. Nat Comput 10, 1195.
3. Vasilis Evangelidis, Andrew Adamatzky, et al (2015). ''Slime mould imitates development of Roman roads in the Balkans''. Journal of Archaeological Science: Reports, No. Vol. 2: 264 - 281.
4. Adamatzky, Andrew. (2012). Slime Mold Solves Maze in One Pass, Assisted by Gradient of Chemo-Attractants. IEEE transactions on nanobioscience. 11. 131-4.
5. Adamatzky, Andrew (2012). Slime mould computes planar shapes. International Journal of Bio-Inspired Computation 4, pp. 149–154.
6. Shimin Li, Huiling Chen, et al. (2020), Slime mould algorithm: A new method for stochastic optimization, Future Generation Computer Systems, Volume 111, 300-323.
7. Wilensky, Uri \& Rand, William (2015). An Introduction to Agent-Based Model. The MIT Press. United States .
8. Sayama, Hiroki (2015). Introduction to the Modeling and Analysis of Complex Systems. Open SUNY Textbooks. New York.
9. Jones, Jeff (2015). From Pattern Formation to Material Computation. Springer International Publishing Switzerland. 
10. Wilensky, U. (1999). NetLogo. [http://ccl.northwestern.edu/netlogo](http://ccl.northwestern.edu/netlogo). Center for Connected Learning and Computer-Based Modeling, Northwestern University, Evanston, IL. 





