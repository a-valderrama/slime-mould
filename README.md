# <u>An agent-based model to analyze the dynamic and the computational properties of the Slime Mould</u> 

Undergraduate thesis project in Computer Science at UNAM.

## About 

The Slime Mould, better known as *Physarum Polycephalum*, is a giant single-celled organism; therefore it has no brain neither neurons, although it shows intelligence capability. The most known experiments are regarding to its capacity of finding the shortest path in a graph and solving different kind of mazes. It has been also demonstrated that Slime Mould successfully approximates spatial representations of problems regarding with computational geometry; like: the generation of Voronoi diagrams, collision-free path planning, Delaunay triangulation, spanning trees, among others.

The **aim** of this project is to develop an **agent-based model** inspired by the behavior of the Physarum Polycephalum in its plasmodium phase to replicate its dynamics. This will be done in the **NetLogo** development environment. 

You can find further information in the [description](https://gitlab.com/a-valderrama/slime-mould/-/blob/master/description.md) file.

## Interface brief

### Collaborators

- Alejandro  T. Valderrama Silva ([at.valderrama@ciencias.unam.mx](at.valderrama@ciencias.unam.mx))
- *Advisor:* Gustavo Carreón Vázquez ([gcarreon@unam.mx]())
