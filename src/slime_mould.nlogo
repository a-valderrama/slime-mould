extensions [
  csv
  py
]

turtles-own[
  energy                 ;; simulates life
  avg-heading            ;; average heading of the scout along his trajectory
  previous-path          ;; last agent connected to the current one
  branch-id              ;; id of each branch -> different for each scout
]

breed [ nuclei nucleus ]
breed [ paths path ]

nuclei-own [
  scout                  ;; defines the scout state of the nuclei
  settler                ;; defines the state of the nuclei inside a cumulus food
  settler2scout          ;; temporary state to move away the cumulus food
  quiescence             ;; defines the satate of a nuclei moments before dying
  quiescence-time        ;; stores the time of an nucleus in a quiescence state
  paths-cluster          ;; paths near within certain range
  nuclei-cluster         ;; nuclei near within certain range
  nearest-scout          ;; nearest scout within certain distance
  nearest-path           ;; neares path within certain distance
  fork-frequency         ;; frequency distance where the scout will fork his branch
  zone-id                ;; id that defines the attraction zone already explored
]

paths-own[
  propagation            ;; propagate available food -> becomes 0 after spread
  pack-id                ;; store the id of the package-food received
]

patches-own[
  oat                    ;; differentiate food-patches
  food-attraction        ;; simulates natural chemical segregation - by food (oats)
  is-obstacle            ;; simulates obstacles for the slime - main for maze experiments
  center                 ;; center of the food cumulus
  attraction-id          ;; id that defines the attraction zone of the oat
  cumulus-id             ;; id that defines the food cumulus
]

globals [
  rho                    ;; represents the density threshold
  stable-graph           ;; store the number of paths and ticks to determine if it is a stable graph/structure
  food-id                ;; global counter for the cumulus-food id
]


;---MAIN PROCEDURES---;

to setup
  clear-all
  reset-ticks
  ; setup py extension
  py:setup py:python
  py:run "from datetime import datetime"
  ; stablish the seed for unique execution
  random-seed new-seed
  create-nuclei nuclei-quantity
  [ set scout true
    set color red
    set size 5
    set energy initial-energy
    set branch-id who
    set previous-path -1]
  ; sort the initial nuclei heading all (360) directions
  let i 0
  let heading-global 0
  while [ i < nuclei-quantity][
    ask turtle i [
        set heading heading-global
      ]
    set i i + 1
    set heading-global heading-global + (360 / nuclei-quantity)
  ]
  ; choose the food pattern
  set food-id nuclei-quantity
  if pattern = "Tokyo Center" [
    tokyo-center-configuration
  ]
  if pattern = "Tokyo" [
    tokyo-configuration
  ]
  if pattern = "Random" [
    random-oats-configuration
  ]
  if pattern = "Simple test" [
    simple-test-configuration
  ]
  if pattern = "Trail" [
    trail-configuration
  ]
  if pattern = "Simple Maze" [
    simple-maze
  ]
  ; initialize slime center
  initialize-center
  ; initialize density
  let area pi * (avoidance-distance ^ 2)
  set rho area * rho-percent
  ; 0 -> num of paths | 1 -> num of ticks
  set stable-graph (list 0 0)
end

to go
  ifelse count nuclei > 0 [
    ; ----- GROWTH PROCESS -----
    ; process for nuclei-scouts
    ask nuclei with [ scout = true and quiescence = false ][
      ifelse ticks > offset-ticks [
        ; define close borders
        check-correct-position
        if energy <= 0 [
          set quiescence true
          stop
        ]
        let local-density count paths with [distance myself <= avoidance-distance]
        ; consider the distance advanced by forked child-branches
        let obstacles-near count patches with [ distance myself <= 7 and is-obstacle = 1 ]
        explore-environment
        if not scout [ stop ]
        if fork-frequency <= 1 [
          if local-density < rho and obstacles-near = 0 [
            fork
          ]
          set fork-frequency random-normal forking-frequency 5
          if fork-frequency < 0 [ set fork-frequency forking-frequency ]
        ]
        ; only scouts move around unexplore area
        move-scout local-density
      ] [ explore-environment
        update-scout-values
        fd 1 ]
    ]
    ; process when within a food cumulus
    ask nuclei with [ settler = true ][
      move-settler
    ]
    ; process when leaving a food cumulus
    ask nuclei with [ settler2scout = true ] [
      move-settler2scout
    ]
    ; process for when scouts run out of food
    ask nuclei with [ scout = true and quiescence = true ][
      if quiescence-time >= initial-energy * quiescence-downtime [ die ]
      set quiescence-time quiescence-time + 1
    ]
    ; process for spreading "food-package" through the branch
    ask paths with [ propagation > 0 ] [
      spread-food-growth
      set color green
      ; stop the food flux, only scouts will start the flux
      set propagation 0
    ]

    ; clean remaining propagation value
    if count nuclei with [ scout = true ] = 0 [
      ask paths [
        set propagation 0
        set color yellow
      ]
    ]

  ] [;; ----- MADURATION PROCESS -----
    ; begin the food-spreading process
    if ticks mod food-package-frequency = 0 [
      ; send another food pulse
      deliver-new-food-package
    ]
    ; spreads new "food-package" through the branch
    ask paths with [ propagation > 0 ] [
      spread-food-maduration
      ; stop the food flux propagation
      set propagation 0
      set color pack-id + 8
    ]
    ; determine if the graph/structure is stable
    ifelse item 0 stable-graph = count paths [
      ; if same num of paths -> increase counter
      set stable-graph (list (count paths) (item 1 stable-graph + 1))
    ] [ set stable-graph (list (count paths) 0) ]
    if item 1 stable-graph >= 180 [
      if export-csv [
        show  "--------Export CSV of STABLE GRAPH--------"
        export-links "stable-graph"
      ]
      if get-vertex [
        show  "--------Export CSV of ADJACENCIES--------"
        export-vertex "adjacencies-stable-graph"
      ]
      stop
    ]
    ; consume energy until death and eliminate useless paths
    ask paths [
      set energy energy - 1
      if energy <= 0 [ die ]
      if critical-branch-situation [
        if count link-neighbors < 2 and [oat] of patch-here != 1 [
          die
        ]
      ]
    ]
  ]
  tick
end

;---MOVEMENT PROCEDURES---;

;; Paths creation process. If within food, change to
;; settler.
to explore-environment
  avoid-obstacles
  create-path
  ifelse [food-attraction] of patch-here = 1 [
    set scout false
    set settler true
    set zone-id [attraction-id] of patch-here
  ] [ wiggle ]
  wiggle
end

;; Move behaviour of scouts, nuclei who explore the environment
to move-scout [local-density]
  avoid-obstacles
  avoid-paths local-density
  avoid-scouts
  update-scout-values
  fd 1
end

;; Movement behaviour when close and into a food cumulus.
to move-settler
  ; define the reproduce-threshold
  ;let reproduce-threshold fork-in-cumulus-percentage * energy
  ;wiggle bias the cumulus center or not
  ifelse [oat] of patch-here = 1 [
    ; check the amount of independent branches (density)
    let neighborhood (list paths in-radius (cumulus-radius + 1))
    let n-branches length remove-duplicates first map [ p -> [branch-id] of p ] neighborhood
    let fork-path 0
    ifelse n-branches <= limit-fork-in-cumulus [
      set fork-path clone-inside-cumulus
    ] [ die ]
    eat fork-path
    wiggle
  ] [ let centerx first zone-id
    let centery item 1 zone-id
    let target-heading towardsxy centerx centery
    set heading target-heading
    set energy energy - 1
  ]
  create-path
  update-settler-values
  fd 1
end

;; Move behaviour of scouts, temporary state to move away food
;; cumulus (after eating).
to move-settler2scout
  ; if within food attraction zone, start a scout behaviour
  let attraction-zone [attraction-id] of patch-here
  if [food-attraction] of patch-here = 1 and
   zone-id = attraction-zone [
    wiggle
    set energy energy - 1
  ]
  ; when exit the food attraction zone but entering
  ; a new one
  if [food-attraction] of patch-here = 1 and
   zone-id != attraction-zone [
    set settler2scout false
    set settler true
    set zone-id [attraction-id] of patch-here
  ]
  ; when exiting the food attraction zone
  if [food-attraction] of patch-here = 0 and
   [ oat ] of patch-here = 0 [
    set settler2scout false
    set scout true
  ]
  create-path
  update-settler-values
  avoid-obstacles
  fd 1
end

;;Construct the veins of the Slime, with the paths breed
to create-path
  let id branch-id
  let last-path previous-path
  let son-id 0
  hatch-paths 1 [
    ; the new path is the last created by the scout
    set son-id who
    set shape "dot"
    set size path-size
    set color yellow
    set branch-id id
    set pack-id -1
    create-link-to path last-path
    set previous-path last-path
  ]
  set previous-path son-id
end

;---FEEDING PROCEDURES--;

;; Eating procedure of a scout. Gain enough energy to match its
;; initial energy.
to eat [ fork-path ]
  ;restore the scout's energy
  set energy initial-energy
  ; redefine itself to try to exit the food area
  set settler false
  set settler2scout true
  ; Store the path who spread the food
  let spread-path previous-path
  ; create path with branch-id equal to the food cumulus id as
  ; vertex in final graph
  let new-branch-id [cumulus-id] of patch-here
  let last-path previous-path
  let son-id 0
  hatch-paths 1 [
    set son-id who
    set shape "dot"
    set size path-size
    set color yellow
    set branch-id new-branch-id
    set pack-id -1
    create-link-to path last-path
    set previous-path last-path
    ; only for forks inside food cumulus
    if fork-path != 0 [
      create-link-to path fork-path
      ask path fork-path [ set previous-path son-id ]
    ]
  ]
  set previous-path son-id
  ; ghost turtle to retreive a new unique-id for a new
  ; representative edge in the final graph
  let temp 0
  hatch-nuclei 1 [
    set temp who
    set fork-frequency forking-frequency - 5
  ] ask nucleus temp [ die ]
  set branch-id temp
  fd 1
  ; start the spreading food reaction (chain-procedure)
  initial-spread spread-path
end

;; Begin the food spread. This initial path must spread the
;; "food-package" both backwards and forwards
to initial-spread [ spread-path ]
  ask path spread-path [
    set pack-id ticks
    ; update path values
    set propagation food-package-size
    set energy energy + propagation
  ]
end

;; Spread the new food to the adjacent paths,
;; during the growth phase.
to spread-food-growth
  let parent-prop propagation
  ; store parent's pack-id
  let parent-pack-id pack-id
  ; keep in mind the id of branch your food comes from
  let feeder-id branch-id
  ; restore food in parent-scout
  awake-parent-scout parent-prop
  ; neighbors that are already pleased and receive food from
  ; another branch flux
  ask link-neighbors with [ energy >= initial-energy * 0.8 ] [
    set propagation parent-prop - 1
  ]
  ; neighbors that need to eat and receive food from another
  ; package-food flux
  accept-spread-food parent-pack-id parent-prop feeder-id

end

;; Spread the new food to the adjacent paths,
;; during the maduration phase.
to spread-food-maduration
  ; store primogenitor's propagation
  let parent-prop propagation
  ; store parent's pack-id
  let parent-pack-id pack-id
  ; keep in mind the id of branch your food comes from
  let feeder-id branch-id
  ; neighbors that are already pleased and receive food from
  ; another package-food flux
  ask link-neighbors with [ energy >= initial-energy * 0.8 ] [
    ; keep the food flux
    if pack-id != parent-pack-id [
      set pack-id parent-pack-id
      set propagation parent-prop
    ]
  ]
  ; neighbors that need to eat and receive food from another
  ; package-food flux
  accept-spread-food parent-pack-id parent-prop feeder-id
end

;; Receives the food from your local neighbors
to accept-spread-food [ primogenitor-pack-id primogenitor-prop feeder-id ]
  ask link-neighbors [
    ; keep the food flux
    if pack-id != primogenitor-pack-id [
      set pack-id primogenitor-pack-id
      ; give priority to your own branch
      ifelse branch-id != feeder-id [
        accept-spread-food-aux primogenitor-prop * (1 - food-package-priority)
      ] [ accept-spread-food-aux primogenitor-prop ]
    ]
  ]
end

;; Auxiliar function to simulate feeding process on paths.
;; Receives the original propagation value of the path
;; executing the procedure.
to accept-spread-food-aux [ propagation-aux ]
  set propagation propagation-aux - 1
  ; eat from the "food-package"
  set energy energy + propagation
end

;; Send another food package to a path within a cumulus
;; food. In order to reinforce food-paths
to deliver-new-food-package
  ;; Phase 1: Identify the list of all the branches that
  ;; are inside of a food cumulus.
  let ids (list)
  ask paths with [ [oat] of patch-here = 1 ] [
    if not member? branch-id ids [ set ids fput branch-id ids ]
  ]

  ;; Phase 2: Start the spreading a new "package-food" in each
  ;; of the branches identified.
  foreach ids [ x -> ask n-of 1 paths with [ [oat] of patch-here = 1 and branch-id = x ] [
      ; update path values
      set propagation food-package-size
      set pack-id ticks
      if energy <= initial-energy * 0.8 [
        set energy energy + propagation
      ]
    ]
  ]
end

;; Reproduce inside a cumulus food to ensure further
;; environment exploring.
to-report clone-inside-cumulus
  let p random-float 1
  let new-heading 0
  ifelse p <= 0.5 [
    ; offset to the left
    set new-heading avg-heading - (random-normal fork-offset 5)
  ] [
    ; offset to the right
    set new-heading avg-heading + (random-normal fork-offset 5)
  ]
  let path-id 0
  hatch-nuclei 1 [
    let current-scout-id who
    set heading new-heading
    set fork-frequency random-normal forking-frequency 5
    if fork-frequency < 0 [ set fork-frequency forking-frequency ]
    set energy 100
    set branch-id who
    set settler false
    set settler2scout true
    update-settler-values
    check-correct-position
    fd 1
    ; create a path linked to the fork
    hatch-paths 1 [
      set path-id who
      ask nucleus current-scout-id [
        set previous-path path-id
      ]
      set shape "dot"
      set size path-size
      set color yellow
      set pack-id -1
    ]
    fd 1
  ]
  report path-id
end

;---COLECTIVE PATTERN MOVEMENT PROCEDURES---;

;; Fork the necessary branches. All forks take place at
;; the tip of the branch.
to fork
  let p random-float 1
  let new-heading 0
  let offset random-normal fork-offset 5
  ; define the heading of the new branch
  ifelse p <= 0.5 [
    ; offset to the left
    set new-heading heading - offset
  ] [ ; offset to the right
      set new-heading heading + offset ]
  ; create the new branch leader
  hatch-nuclei 1 [
    avoid-obstacles
    let current-scout-id who
    set heading new-heading
    set branch-id who
    ; avoid immediate forking
    set fork-frequency forking-frequency * 1.1
    let i 0
    while [ i < 5  ] [
      update-scout-values
      let last-path previous-path
      fd 1
      ; check if it's in a food zone
      if [food-attraction] of patch-here = 1 [
        set scout false
        set settler true
        set zone-id [attraction-id] of patch-here
      ]
      check-correct-position
      avoid-obstacles
      hatch-paths 1 [
        let my-id who
        ask nucleus current-scout-id [
          set previous-path my-id
        ]
        set shape "dot"
        set size path-size
        set color yellow
        set pack-id -1
        create-link-to path last-path
        set previous-path last-path
      ]
      set i i + 1
    ]
    ; advance 1 more step to separate from the last path created
    update-scout-values
    fd 1
    ; if the fork hits another branch, it "merges"
    if any? paths-on patch-ahead 1 [ die ]
  ]
end

;; Avoid paths by alterate the heading
to avoid-paths [ local-density ]
  ; check the paths in a custom neighborhood
  let aux-cluster other paths in-cone avoidance-distance angle-vision
  ; remove behind, bugged, agent
  let bugged-nearest min-one-of aux-cluster [distance myself]
  ask bugged-nearest [ set aux-cluster other aux-cluster ]
  set paths-cluster aux-cluster
  if count paths-cluster >= 2 [
    set paths-cluster min-n-of 2 paths-cluster [distance myself]
    ; Get the points to defines the abstract segments
    let p one-of paths-cluster
    let p2 one-of paths-cluster with [who != [who] of p]
    ; merge only when passed the offticks
    if ticks > offset-ticks [
      ; check the environment density
      if local-density >= rho [
        merge p bugged-nearest
      ]
    ]
    ; avoid agents
    avoid p p2 true
  ]
end

;; Avoid other scouts by alterate the heading
to avoid-scouts
  ; scouts around myself
  let neighborhood  other nuclei with [scout = true] in-radius avoidance-distance
  ; Avoid nuclei neighbors
  if any? neighborhood  [
    find-nearest-neighbor neighborhood
    if distance nearest-scout < avoidance-distance [
      ; second point of the segment to collide with
      find-second-point
      let p2 [ nearest-path ] of nearest-scout
      ; avoid agents
      avoid nearest-scout p2 true
    ]
  ]
end

;; Avoid the obstacles in the environment by alterate
;; the heading simulating reflection.
to avoid-obstacles
  let obstacles-ahead patches in-cone (avoidance-distance * 2.5) angle-vision
   with [ is-obstacle = 1 ]
  if count obstacles-ahead >= 2 [
    ; Get the points to defines the abstract segments
    let p one-of obstacles-ahead
    let x [pxcor] of p
    let y [pycor] of p
    let p2 one-of obstacles-ahead with [pxcor != x or pycor != y ]
    ; avoid patches
     avoid p p2 false
  ]
  ; when fully in limit, die
  let inside-limit count neighbors with [is-obstacle = 1]
  if inside-limit >= 3 [ die ]
end

;; Avoid the imminent collision with others. Receives two agents or patches
;; that defines the (imaginary) collision's segments.
;; case -> defines if the method will be used with agents or patches
;;       true for agents
;;       false for patches
to avoid [ p p2 case ]
  ; Phase 1: Define the new angle
  let offset random-normal angle-turn-offset 5
  let theta 0
  let r random-float 1
  ifelse r <= 0.5 [
    ; offset to the left
    set theta angle-turn + offset
  ] [
    ; offset to the right
    set theta angle-turn - offset
  ]
  ; for patches
  let new-heading heading
  ; for agents
  if case [ set new-heading [ heading ] of p ]

  ; Phase 2: Defines if the agent is going to collide from above
  ; or from below. Also defines the orentation of the turn
  let stand true
  let turn true
  ifelse case [
    set stand collision-stand [xcor] of p [ycor] of p [xcor]
     of p2 [ycor] of p2
    set turn collision-turn-way p stand
  ] [ set stand collision-stand [pxcor] of p [pycor] of p
       [pxcor] of p2 [pycor] of p2
      set turn collision-turn-way self stand ]

  ; Phase 3: Finally alter the scout heading
  ifelse turn [
    ; turn right
    set heading new-heading + theta
  ] [
    ; turn left
    set heading new-heading - theta
  ]
end

to merge [ target last-path ]
  ; adjust heading of current branch to the collision branch
  ask last-path [ set heading towards target ]
  let i 0
  let target-distance [distance myself] of target
  let my-previous-path [previous-path] of last-path
  ; creat paths until collide with the other branch
  while [ target-distance >= 1.65][
    ask last-path [
      ; create a new path and advance one step
      hatch-paths 1 [
        ; your last path is now my last path
        create-link-to path my-previous-path
        set previous-path my-previous-path
        ; update previous path of current path
        set my-previous-path who
        set shape "dot"
      	set size path-size
      	set color yellow
        set pack-id -1
    	]
      fd 1
      set previous-path my-previous-path
      set energy energy - 1
      set target-distance [distance myself] of target
    ]
  ]
  ; create link to last path created
  ask last-path [
    ; clear previous links
    ask my-out-links [ die ]
    create-link-to path previous-path
  ]
  ; tell the branch target that we're merging
  ask target [
    ; no need to update previous path, since a different branch
    create-link-to last-path
  ]
  die ;scout
end

;--- ENVIRONMENT CONFIGURATION--;

;; Initialize food areas simulating tokyo experiment
to tokyo-center-configuration
  ; closest circle in tokyo experiment
  ; tratar de hacer esto con agentsets para ahorrar lineas
  ask patches at-points [ [-8 55] [-25 87] [47 -2]
   [58 55] [80 -30] [-75 15] [-55 -15] [-13 -64]
   [60 -58] [30 -85] ] [ food-init ]

  configure-food-zones
  ; set a food zone in the center of the slime
  if use-food-center = true [
    ask patch 0 0 [ food-init]
    set-nucleus-food 0 0
  ]
end

;; Initialize food areas simulating tokyo experiment
to tokyo-configuration
  ; change the center
  ask nuclei [ set xcor -60.01 set ycor -9.18]

  ; set the oat flakes
  ask patches at-points [ [-228 -220] [35 -41]
   [28 -70] [-9 -102] [-18.3 -211] [91 -162]
   [102 -99] [115 -42] [69 -9] [93 9]
   [137 39] [175 -8] [216 -14] [2 -15]
   [4 30] [160 218] [123 155] [-16 201]
   [-37 147] [61 88] [-206 -123] [-166 -112]
   [-139 -108] [-102 -121] [-74  -134] [-78  -83]
   [-151 -42] [-161 -14] [-225 155] [-206 170]
   [-148 168] [-115 153] [-138 110] [-89  60]
   [-79  32]] [ food-init ]
  ; create limit line
  let limit-points [[192.75 248.86] [173.65 206.65] [153.73 190.73]
    [153.17 148.11] [142.24 145.59] [138.79 100.44] [170.29 46.79]
    [220.20 -4.98] [228.54 -17.35] [218.76 -25.84] [177.05 -24.40]
    [154.76 -38.78] [130.59 -38.21] [130.32 -55.17] [117.65 -70.42]
    [109.31 -104.94] [115.64 -123.63] [110.03 -158.16] [116.07 -165.20]
    [83.85 -178.44] [54.37 -180.16] [36.11 -193.25] [24.60 -196.84]
    [7.92 -215.11] [-9.05 -237.26] [-32.49 -230.21] [-49.75 -229.92]
    [-57.66 -223.16] [-25.44 -215.97] [-34.08 -202.45] [-35.37 -163.33]
    [-21.99 -136.72] [-39.83 -128.38] [-9.05 -89.84] [17.122 -76.18]
    [24.78 -61.22] [19.57 -33.17] [3.32 -27.28] [-11.64 -37.49]
    [-32.35 -41.23] [-45.15 -54.32] [-48.60 -72.29] [-65.00 -77.04]
    [-82.98 -100.05] [-83.69 -106.81] [-62.12 -135.86] [-61.11 -141.90]
    [-71.76 -153.55] [-72.91 -166.78] [-93.47 -171.39] [-91.61 -150.82]
    [-106.85 -129.54] [-136.77 -119.75] [-167.55 -119.32] [-190.99 -134.71]
    [-210.12 -140.47] [-215.58 -165.35] [-227.23 -173.55] [-229.82 -206.05]
    [-212.56 -221.87] [-222.19 -248.90]]
  create-turtles 1 [
    set color white
    set size 6
    create-limits limit-points
  ]

  configure-food-zones
  ; set a food zone in the center of the slime
  if use-food-center = true [
    ask patch -60.01 -9.18 [ food-init ]
    set-nucleus-food -60.01 -9.18
  ]
end

;; Initialize food areas with a choosen pattern so debuging
;; and understanding process is easier
to simple-test-configuration
  ; tratar de hacer esto con agentsets para ahorrar lineas
  ask patches at-points [ [58 -2] [-8 65] [63 65]
   [-70 10] [-10 -70] ] [ food-init ]

  configure-food-zones
  ; set a food zone in the center of the slime
  if use-food-center = true [
    ask patch 0 0 [ food-init ]
    set-nucleus-food 0 0
  ]
end

;; Initialize food areas with a choosen pattern to analyze
;; the ability of the slime to follow paths
to trail-configuration
  ; closest circle in tokyo experiment
  ; tratar de hacer esto con agentsets para ahorrar lineas
  ask patches at-points [ [45 40] [85 85] [125 125]
    [83 180] [-45 -40] [-85 -85] [-125 -125]
    [-165 -165] ] [ food-init ]

  configure-food-zones
  ; set a food zone in the center of the slime
  if use-food-center = true [
    ask patch 0 0 [ food-init]
    set-nucleus-food 0 0
  ]
end

;; Initialize food areas in a random way
to random-oats-configuration
  ; Randomly set the food
  ask n-of cumulus patches [
    type pxcor type "," type pycor type "\n"
    food-init
  ]

  configure-food-zones
  ; set a food zone in the center of the slime
  if use-food-center = true [
    ask patch 0 0 [ food-init]
    set-nucleus-food 0 0
  ]
end

;; Initialize the obstacles area, representing a simple maze
;; to start with.
to simple-maze
  ; create a center in each entry
  let half (ceiling nuclei-quantity / 2)
  let scouts [self] of nuclei
  let center1 sublist scouts 0 half
  let center2 sublist scouts half length scouts
  foreach center1 [ x -> ask x [ set xcor -95 set ycor -62 ]]
  foreach center2 [ x -> ask x [ set xcor 95 set ycor -62 ]]

  ; set the oat flakes
  ask patch -3 57 [ food-init ]
  configure-food-zones

  ; create the maze
  let outside-wall [[-115 -95] [-115 75] [115 75] [115 -95]]
  let inside-wall [[-75 -95] [-75 40] [75 40] [75 -95]]
  let lid1 [[-115 -95] [-75 -95]]
  let lid2 [[115 -95] [75 -95]]
  create-turtles 1 [
    set color white
    set size 6
    create-limits outside-wall
    create-limits inside-wall
    create-limits lid1
    create-limits lid2
    die
  ]

  if use-food-center = true [
    ask patch -95 -62 [ food-init ]
    set-nucleus-food -95 -62
    ask patch 95 -62 [ food-init ]
    set-nucleus-food 95 -62
  ]
end

;--- AUXILIAR PROCEDURES ---;

; Create the Slime's "heart". Also, stablish a unique path number
; for the center paths, so that the center can be identified while
; analysing as a graph.
to initialize-center
  ask nuclei [
    set settler false
    set settler2scout false
    set color blue
    set fork-frequency random-normal forking-frequency 5
    if fork-frequency < 0 [ set fork-frequency forking-frequency]
    set avg-heading heading
    set quiescence false
    set quiescence-time 0
    fd 1
    let id 0
    hatch-paths 1 [
      set id who
      set shape "dot"
      set size path-size
      set color yellow
      set pack-id -1
    ]
    set previous-path id
    fd 1
    set branch-id 1000000
    create-path
    set branch-id who
    fd 1
  ]

end

;; Verify that the scouts are inside the defined
;; world.
to check-correct-position
  if xcor <= min-pxcor + 1 or xcor >= max-pxcor - 1 [ die ]
  if ycor <= min-pycor + 1 or ycor >= max-pycor - 1 [ die ]
end

;; Updates the fork-frequency, average-heading and energy
;; from a scout.
to update-scout-values
  set avg-heading (avg-heading + heading) / 2
  set fork-frequency fork-frequency - 1
  if fork-frequency < 0 [ set fork-frequency 0 ]
  set energy energy - 1
end

;; Updates the average-heading and fork-frequency
;; from a settler.
to update-settler-values
  set avg-heading (avg-heading + heading) / 2
  set fork-frequency random-normal (floor forking-frequency / 3) 1
  if fork-frequency < 0 [ set fork-frequency forking-frequency]
end

;; Random walk (exploration)
to wiggle
  rt random-float wiggle-angle - random-float wiggle-angle
end

;; While spreading the "food-package", feeds the parent-scout
;; that is in a quiescence state.
to awake-parent-scout [ temp ]
  let father-scout nuclei-on patch-ahead 1
  if any? father-scout [
    ask father-scout with [ quiescence = true ][
      set energy energy + temp
      set quiescence false
      set quiescence-time 0
    ]
  ]
end

;; Defines if the agent is going to collide from above
;; or from below. Recives the points that define the collision
;; segments. above = True | below = False
to-report collision-stand [ x y x2 y2 ]
  ; coordinate of the agent (nucleus) we are considering
  let x0 xcor
  let y0 ycor

  ; adjust points for avoiding division by zero
  let y-aux (y + 0.0001)
  let x2-aux (x2 + 0.0001)

  ; Elements needed to establish the segment's equation (y = mx + b)
  let m (y-aux - y2) / (x - x2-aux)
  let b (- x) * m + y-aux

  ; defines above/below
  ifelse y0 >= m * x0 + b [
    report true
  ] [ report false ]
  ;if y0 < m * x0 + b [ report false]
end

;; Use the conventional 1-argument tan
to-report arctan [x]
  report atan x 1
end

;; Defines if the turn angle of the collision is going to be
;; to the rigth or to the left. Receives the stand of the scout.
;; right-turn = True | left-turn = False.
to-report collision-turn-way [ p stand ]
  let hp [heading] of p

  ; First check the quadrant of the paths line
  if hp >= 0 and hp <= 180 [
    ifelse stand [
      report false
    ] [ report true ]
  ]

  if hp > 180 [
    ifelse stand [
      report true
    ] [ report false ]
  ]
end

;; Find the closest nuclei-neighbor within a redius
to find-nearest-neighbor [ neighborhood   ]
  set nearest-scout min-one-of neighborhood [distance myself]
end

;; Set the nearest path to the scout at issue as the second point
;; to define the segment needed.
to find-second-point
  ask nearest-scout [
    set nearest-path min-one-of paths in-radius 1.5 [distance myself]
  ]
end

;; Defines if the heading angle of the angent is perpendicular
to-report is-heading-perpendicular [ p p2 s2 ]
  ; Phase 1: Define the slope of both segments. Ensure we never get a division by zero
  let mp (([ycor] of p + 0.00001) - [ycor] of p2) / (([xcor] of p + 0.00001) - [xcor] of p2)
  let ms ((ycor + 0.00001) - [ycor] of s2) / ((xcor + 0.00001) - [xcor] of s2)

  ; Phase 2: Verify if ms and mp are (aprox) negative reciprocals to each other
  let neg-reciprocals-offset 0.1
  let ms-reciprocal (ms ^ (- 1)) * (- 1)
  ifelse mp < ms + neg-reciprocals-offset and mp > ms - neg-reciprocals-offset [
    report true
  ] [ report false ]
end

;; Set the food-cumulus' center
;; Patch procedure
to food-init
  set oat 1
  set center true
  set cumulus-id food-id + 1
  set food-id cumulus-id
end

;; Assign the defult values for the food-patches
to configure-food-zones
  ask patches with [ oat = 1 ][
    let temp-id cumulus-id
    ask patches in-radius cumulus-radius [
      set oat 1
      set pcolor 34
      let id patches in-radius 5 with [center = true]
      set cumulus-id temp-id
    ]
  ]
  ; Stablish the attraction area for the scout-nucleus
  ask patches with [ center = true ] [
    let id list pxcor pycor
    ask patches with [distance myself = cumulus-radius] [
      ask patches in-radius attraction-radius with [ oat != 1] [
        if attraction? [
          set pcolor 129
        ]
        set food-attraction 1
        set attraction-id id ]]]
end

;; Set a food cumulus in the origin of the organism
to set-nucleus-food [ x y ]
  ask patch x y [
    ask patches in-radius (cumulus-radius + 5) [
      set oat 1
      set pcolor yellow
    ]
  ]
end

;; Initialize obstacle-patches variables
;; Patch procedure
to obstacle-init
  set is-obstacle 1
end

;; Create the obstacles used in the scenarios
to create-limits [ limit-points ]
  while [length limit-points >= 2][
    ; stablish the begining of the segment
    set xcor first first limit-points
    set ycor item 1 first limit-points
    let xaim first item 1 limit-points
    let yaim item 1 item 1 limit-points
    ; go towards next point
    set heading ( towards patch xaim yaim)
    let segment-length sqrt ((xcor - xaim) ^ 2 + (ycor - yaim) ^ 2 )
    let i 0
    ; draw the limit line
    while [i <= segment-length] [
      ask patches in-radius 1 [
        set is-obstacle 1
        set pcolor white
      ]
      forward 1
      set i i + 1
    ]
    set limit-points remove-item 0 limit-points
  ]
end

;; Assign the defult values for the obstacle-patches
to create-obstacle-vertical [ xvalue yvalue ]
  let line-increment 1
  let new-p-yvalue 0
  let new-n-yvalue 0
  while [ line-increment < 80 ][
    set new-p-yvalue yvalue + line-increment
    set new-n-yvalue yvalue - line-increment
    ; create line upwards
    ask patch xvalue new-p-yvalue [
      obstacle-init
    ]
    ; create line downwards
    ask patch xvalue new-n-yvalue [
      obstacle-init
    ]
    set line-increment line-increment + 1
  ]
end

;; Export links CSV file with
to export-links [ name ]
  let l (list)
  set l fput [ "end1" "end2" ]
    [ (list [ who ] of end1 [ who ] of end2) ] of links
  let id (py:runresult "datetime.now().strftime(\"%d-%m-%Y_%H-%M-%S\")" )
  csv:to-file (word name "_" id ".csv")  l
end


;; Export a CSV file with
; DEJAR QUE SÓLO CREE LA LISTA DE ADYACENCIAS DE PYTHON
to export-vertex [ name ]
  py:set "name" name
  let id -1
  let adjacencies -1
  let links-list (list)
  ; use a dictionary to remove repetitions
  py:run "edges = dict()"
  let file-id (py:runresult "datetime.now().strftime(\"%d-%m-%Y_%H-%M-%S\")" )
  ;file-open (word name "_" file-id ".txt")
  ;file-type "id1" file-type "\t" file-type "id2" file-type "\n"
  ask paths [
    set id branch-id
    let vertex who
    py:set "id" branch-id
    ; paths' neighbors, i.e., graph's vertex
    set adjacencies paths in-radius 1.5 with [branch-id != id and link-neighbor? turtle vertex ]
    if any? adjacencies [
      ask adjacencies [
        set color red
        py:set "branch_id" branch-id
        ;file-write id file-type "\t" file-write branch-id file-type "\n"
        set links-list lput (list id branch-id) links-list
        ; add all the vertex to the dict
        (py:run
          "if id not in edges:"
          "    edges[id] = list()"
          "if branch_id not in edges:"
          "    edges[branch_id] = list()"
        )
      ]
    ]
  ]
  ; clean the links' list -> save them in a dict
  foreach links-list[ l ->
    py:set "k" item 0 l
    py:set "v" item 1 l
    (py:run
      ; the dict already contains all the keys
      "if v not in edges[k] and k not in edges[v]:"
      "    edges[k].append(v)"
    )
  ]
  (py:run
    "file_name = name + '_' + 'clean_' + datetime.now().strftime(\"%d-%m-%Y_%H-%M-%S\") + '.txt'"
    "with open(file_name, 'w', encoding='utf-8') as file:"
    "    file.write('id1' + '\t' + 'id2' + '\\n')"
    "    for key in edges:"
    "        for v in edges[key]:" ; constant values here
    "            file.write(str(key) + \"\t\" + str(v) + '\\n')"
  )
  file-close-all
end
@#$#@#$#@
GRAPHICS-WINDOW
352
10
1187
720
-1
-1
1.4
1
10
1
1
1
0
1
1
1
-295
295
-250
250
0
0
1
ticks
30.0

BUTTON
126
50
189
83
go
go
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
45
50
108
83
setup
setup
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SLIDER
61
10
261
43
nuclei-quantity
nuclei-quantity
0
400
25.0
1
1
NIL
HORIZONTAL

TEXTBOX
106
85
256
104
Nuclei behaviour
15
124.0
1

SLIDER
63
184
266
217
wiggle-angle
wiggle-angle
0
90
16.0
1
1
°
HORIZONTAL

SLIDER
63
221
267
254
avoidance-distance
avoidance-distance
0
10
2.0
0.25
1
patches
HORIZONTAL

BUTTON
205
50
279
83
NIL
go
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SLIDER
63
294
267
327
angle-vision
angle-vision
0
270
50.0
5
1
°
HORIZONTAL

SLIDER
64
147
266
180
offset-ticks
offset-ticks
0
50
20.0
1
1
ticks
HORIZONTAL

TEXTBOX
1329
164
1526
202
Environment configuration
15
124.0
1

SLIDER
1216
260
1398
293
cumulus
cumulus
0
15
6.0
1
1
#quantity
HORIZONTAL

SLIDER
1216
303
1397
336
cumulus-radius
cumulus-radius
0
10
5.0
1
1
patches
HORIZONTAL

SLIDER
64
258
267
291
rho-percent
rho-percent
0
1
0.3
0.1
1
percent
HORIZONTAL

SLIDER
62
331
268
364
angle-turn
angle-turn
0
90
60.0
5
1
°
HORIZONTAL

SLIDER
62
368
269
401
angle-turn-offset
angle-turn-offset
0
90
30.0
5
1
°
HORIZONTAL

SLIDER
62
404
269
437
forking-frequency
forking-frequency
0
100
25.0
5
1
ticks
HORIZONTAL

SLIDER
62
440
268
473
fork-offset
fork-offset
0
180
90.0
5
1
°
HORIZONTAL

SLIDER
1216
345
1399
378
attraction-radius
attraction-radius
0
25
18.0
1
1
patches
HORIZONTAL

SWITCH
1246
212
1352
245
attraction?
attraction?
1
1
-1000

CHOOSER
1422
202
1530
247
pattern
pattern
"Tokyo Center" "Tokyo" "Random" "Simple test" "Trail" "Simple Maze" "Alone"
1

SLIDER
65
108
265
141
initial-energy
initial-energy
0
500
250.0
10
1
ticks
HORIZONTAL

PLOT
1599
285
1882
461
Scouts
time
scouts
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 0 -5825686 true "" "plot count nuclei "

SLIDER
62
550
267
583
food-package-size
food-package-size
0
500
155.0
5
1
NIL
HORIZONTAL

SLIDER
62
587
267
620
food-package-priority
food-package-priority
0
1
0.15
0.01
1
NIL
HORIZONTAL

TEXTBOX
1377
411
1474
430
Testing\n
15
124.0
1

SLIDER
1238
440
1410
473
energy-threshold
energy-threshold
0
200
120.0
1
1
NIL
HORIZONTAL

BUTTON
1429
441
1538
474
select-paths
ask paths with [ energy >= energy-threshold ] [ set color red ]
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
1429
483
1538
516
unselect-paths
ask paths with [ color = red] [ set color green ]
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
1429
524
1539
557
get-network
ask paths with [ color != red ] [ set color black ]
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
1428
566
1540
599
get-organism
ask paths with [ color = black ] [ set color yellow ]
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

MONITOR
1295
477
1352
522
scouts
count nuclei with [ scout = true ]
17
1
11

SLIDER
62
475
268
508
quiescence-downtime
quiescence-downtime
0
1
0.3
0.1
1
percent
HORIZONTAL

SLIDER
62
626
268
659
food-package-frequency
food-package-frequency
0
140
50.0
5
1
ticks
HORIZONTAL

PLOT
1598
84
1880
262
Energy histogram
energy
frequency 
0.0
200.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 1 -14070903 true "" "histogram [energy] of paths"

SWITCH
1270
528
1386
561
export-csv
export-csv
1
1
-1000

SWITCH
1415
259
1550
292
use-food-center
use-food-center
0
1
-1000

PLOT
1599
498
1882
675
Paths
time
paths
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 0 -15040220 true "" "plot count paths"

SLIDER
62
512
268
545
limit-fork-in-cumulus
limit-fork-in-cumulus
0
10
6.0
1
1
NIL
HORIZONTAL

TEXTBOX
1712
30
1862
49
Metrics
15
124.0
1

SWITCH
1237
566
1422
599
critical-branch-situation
critical-branch-situation
0
1
-1000

SWITCH
1265
607
1408
640
get-vertex
get-vertex
1
1
-1000

SLIDER
1417
304
1576
337
path-size
path-size
0
10
1.0
1
1
patches
HORIZONTAL

@#$#@#$#@
## WHAT IS IT?

(a general understanding of what the model is trying to show or explain)

## HOW IT WORKS

(what rules the agents use to create the overall behavior of the model)

## HOW TO USE IT

(how to use the model, including a description of each of the items in the Interface tab)

### Params description:
#### Nuclei's behaviour:
- intial-energy: Energy with which the agents begin
- offset-ticks: The amount of time-ticks that the model is going to wait to begin the slime's growth stage
- wiggle-angle: A random angle that defines a random walk
- avoidance-distance: The radius (patches) that defines the circumference for the minimum density of agents to avoid at any moment
- rho-percent: The percentage that defines the minimum density of agents, in the given circumference, to decide if it can generate a fork or not
- angle-vision: The angle of vision of the agents
- angle-turn: Defines the amount of change in the angle that the agents make to avoid their equals
- angle-turn-offset: An angle offset for the angle-turn value. This will create a random variation.
- forking-frequency: Defines the frequency (ticks) with which the fork happens
- fork-offset: An angle offset for the angle value of the agent after the forking process. This will create a random variation
- inherited-energy: The amount of energy that is reduce from a forked branch
- reproduce-percentage: Defines the threshold that decides if the agents can reproduce or not
- food-package-size: The amount of energy that is going to be spread accross the Slime
- food-package-priority: The amount of energy reduced when an agent eat a package-food
- quiescence-downtime: Defines the time of quiescence time allowed before the agent dies
- food-package-frequency: Defines th frequency with which the food-packages are going to be sent
#### Food configuration:
- attraction?: On -> shows the attraction circumferece from a pile of food
- pattern: You can choose what ever configuration you want
- cumulus: Amount of piles of food that there are going to be
- cumulus-radius: Radius of the food of pile to set the attraction area
- attraction-radius: Sets the circumference to attract the agents
- use-food-center: Decide if a food cumulus is going to be set in the center of the model

## THINGS TO NOTICE

(suggested things for the user to notice while running the model)

## THINGS TO TRY

(suggested things for the user to try to do (move sliders, switches, etc.) with the model)

## EXTENDING THE MODEL

(suggested things to add or change in the Code tab to make the model more complicated, detailed, accurate, etc.)

## NETLOGO FEATURES

(interesting or unusual features of NetLogo that the model uses, particularly in the Code tab; or where workarounds were needed for missing features)

## RELATED MODELS

(models in the NetLogo Models Library and elsewhere which are of related interest)

## CREDITS AND REFERENCES

(a reference to the model's URL on the web if it has one, as well as any other necessary credits, citations, and links)
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

sheep
false
15
Circle -1 true true 203 65 88
Circle -1 true true 70 65 162
Circle -1 true true 150 105 120
Polygon -7500403 true false 218 120 240 165 255 165 278 120
Circle -7500403 true false 214 72 67
Rectangle -1 true true 164 223 179 298
Polygon -1 true true 45 285 30 285 30 240 15 195 45 210
Circle -1 true true 3 83 150
Rectangle -1 true true 65 221 80 296
Polygon -1 true true 195 285 210 285 210 240 240 210 195 210
Polygon -7500403 true false 276 85 285 105 302 99 294 83
Polygon -7500403 true false 219 85 210 105 193 99 201 83

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

wolf
false
0
Polygon -16777216 true false 253 133 245 131 245 133
Polygon -7500403 true true 2 194 13 197 30 191 38 193 38 205 20 226 20 257 27 265 38 266 40 260 31 253 31 230 60 206 68 198 75 209 66 228 65 243 82 261 84 268 100 267 103 261 77 239 79 231 100 207 98 196 119 201 143 202 160 195 166 210 172 213 173 238 167 251 160 248 154 265 169 264 178 247 186 240 198 260 200 271 217 271 219 262 207 258 195 230 192 198 210 184 227 164 242 144 259 145 284 151 277 141 293 140 299 134 297 127 273 119 270 105
Polygon -7500403 true true -1 195 14 180 36 166 40 153 53 140 82 131 134 133 159 126 188 115 227 108 236 102 238 98 268 86 269 92 281 87 269 103 269 113

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270
@#$#@#$#@
NetLogo 6.2.2
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180
@#$#@#$#@
0
@#$#@#$#@
